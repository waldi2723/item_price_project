package com.example.controller;

import com.example.domain.Item;
import com.example.dto.ItemDTO;
import com.example.service.ItemServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by wstawinski on 24.02.2017.
 */
@RestController
@RequestMapping("item")
public class ItemController {

    @Autowired
    private ItemServiceImpl itemService;

    @GetMapping
    public List<ItemDTO> getAllItems() {
        return this.itemService.findAll();
    }

    @GetMapping("{id}")
    public ItemDTO getItemById(@PathVariable(name = "id") String id) {
        return this.itemService.findById(id);
    }


    @PutMapping("{id}")
    public void updateItem(@RequestBody Item item, @PathVariable(name = "id") String id) {
        this.itemService.updateItem(item,id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createItem(@RequestBody Item item) {
        this.itemService.createItem(item);
    }

}
