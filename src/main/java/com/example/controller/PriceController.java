package com.example.controller;

import com.example.domain.Item;
import com.example.domain.Price;
import com.example.dto.ItemDTO;
import com.example.dto.PriceDTO;
import com.example.service.ItemServiceImpl;
import com.example.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by wstawinski on 27.02.2017.
 */
@RestController
@RequestMapping("/price")
public class PriceController {
    @Autowired
    private PriceService priceService;

    @GetMapping
    public List<PriceDTO> getAllPrices() {
        return priceService.findAll();
    }

    @GetMapping("{id}")
    public PriceDTO getPriceById(@PathVariable(name = "id") String id) {
        return this.priceService.findById(id);
    }


    @PutMapping("{id}")
    public void updatePrice(@RequestBody Price price, @PathVariable(name = "id") String id) {
        this.priceService.updatePrice(price, id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createItem(@RequestBody Price price) {
        this.priceService.createItem(price);
    }


}
