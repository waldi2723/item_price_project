package com.example.service;

import com.example.domain.Item;
import com.example.dto.ItemDTO;

import java.util.List;

/**
 * Created by wstawinski on 24.02.2017.
 */
public interface ItemService {



    List<ItemDTO> findAll();

    ItemDTO findById(String id);

    void createItem(Item item);

    void updateItem(Item item, String id);
}
