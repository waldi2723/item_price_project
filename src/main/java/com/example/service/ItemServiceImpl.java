package com.example.service;

import com.example.domain.Item;
import com.example.dto.ItemDTO;
import com.example.mapper.ItemMapper;
import com.example.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wstawinski on 24.02.2017.
 */
@Service
public class ItemServiceImpl implements ItemService {


    private ItemRepository itemRepository;
    private ItemMapper itemMapper;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository, ItemMapper itemMapper) {
        this.itemRepository = itemRepository;
        this.itemMapper = itemMapper;
    }

    @Override
    public List<ItemDTO> findAll() {
        List<Item> items = this.itemRepository.findAll(); // tutaj wyszukuje w bazie i tworze encje

        List<ItemDTO> ret = itemMapper.fromEntityList(items);
        return ret;


    }

    @Override
    public ItemDTO findById(String id) {
        Long intId = Long.parseLong(id);
        Item item = this.itemRepository.findByItemId(intId); //przeszukaj w bazie po ID i przypisz
        return this.itemMapper.fromEntity(item);
    }

    @Override
    public void createItem(Item item) {
    this.itemRepository.save(item);
    }

    @Override
    public void updateItem(Item item, String id) {
        item.setItemId(Long.parseLong(id));
        itemRepository.save(item);
    }
}