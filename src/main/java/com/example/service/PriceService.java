package com.example.service;

import com.example.domain.Item;
import com.example.domain.Price;
import com.example.dto.ItemDTO;
import com.example.dto.PriceDTO;

import java.util.List;

/**
 * Created by wstawinski on 24.02.2017.
 */
public interface PriceService {
    List<PriceDTO> findAll();

    PriceDTO findById(String id);

    void createItem(Price price);

    void updatePrice(Price price, String id);
}
