package com.example.service;

import com.example.domain.Price;
import com.example.dto.PriceDTO;
import com.example.mapper.PriceMapper;
import com.example.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wstawinski on 24.02.2017.
 */
@Service
public class PriceServiceImpl implements PriceService {

    @Autowired
    public PriceServiceImpl(PriceRepository priceRepository, PriceMapper priceMapper) {
        this.priceRepository = priceRepository;
        this.priceMapper = priceMapper;
    }

    private PriceRepository priceRepository;

    private PriceMapper priceMapper;


    public List<PriceDTO> findAll() {
        List<Price> prices = priceRepository.findAll();
        return this.priceMapper.fromEntityList(prices);
    }

    @Override
    public PriceDTO findById(String id) {
        Price price = priceRepository.findByPriceId(Long.parseLong(id));
        return priceMapper.fromEntity(price);
    }

    @Override
    public void createItem(Price price) {

        this.priceRepository.save(price);
    }

    @Override
    public void updatePrice(Price price, String id) {
    price.setPriceId(Long.parseLong(id));
    priceRepository.save(price);

    }
}
