package com.example.mapper;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wstawinski on 24.02.2017.
 */

public interface GenericMapper<E,D> {
    D fromEntity(E entity);

    E fromDto(D dto);

    default List<D> fromEntityList(List<E> entities) {
        return entities.stream()
                .map(this::fromEntity)
                .collect(Collectors.toList());
    }

}
