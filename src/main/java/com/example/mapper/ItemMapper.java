package com.example.mapper;

import com.example.domain.Item;
import com.example.domain.Price;
import com.example.dto.ItemDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by wstawinski on 24.02.2017.
 */
@Component
public class ItemMapper implements GenericMapper<Item, ItemDTO> {


    @Autowired
    private PriceMapper priceMapper;

    public ItemDTO fromEntity(Item entity) {

        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setDescription(entity.getDescription());
        itemDTO.setItemId(entity.getItemId());
        itemDTO.setPricesId(priceMapper.fromEntityList(entity.getPrices()));
      //  itemDTO.setPricesId(entity.getPrices().stream().map(a -> a.getPriceId()).collect(Collectors.toList()));

        return itemDTO;
    }

    public Item fromDto(ItemDTO dto) {

        Item item = new Item();
        item.setItemId(dto.getItemId());
        item.setDescription(dto.getDescription());
       // item.setPrices(dto.getPricesId().stream().map()); // ??

 // ??

return item;

    }
}
