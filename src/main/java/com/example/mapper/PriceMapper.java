package com.example.mapper;

import com.example.domain.Price;
import com.example.dto.PriceDTO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * Created by wstawinski on 24.02.2017.
 */
@Component
public class PriceMapper implements GenericMapper <Price, PriceDTO> {
    @Override
    public PriceDTO fromEntity(Price entity) {
        PriceDTO p = new PriceDTO();
        p.setDate(entity.getDate());
        p.setPriceId(entity.getPriceId());
        p.setItem(entity.getItem());
        return p;
    }

    @Override
    public Price fromDto(PriceDTO dto) {

        Price p = new Price();
        p.setPriceId(dto.getPriceId());
        p.setDate(dto.getDate());
        p.setItem(dto.getItem());
        return p;
    }



  /*  public PriceDTO fromEntity(Price entity) {
        ModelMapper model = new ModelMapper();
        return model.map(entity, PriceDTO.class);
    }


    public Price fromDto(PriceDTO dto) {
        ModelMapper model = new ModelMapper();
        return model.map(dto, Price.class);
    }
*/


}
