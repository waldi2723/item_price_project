package com.example.repository;

import com.example.domain.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wstawinski on 23.02.2017.
 */
@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {
    List<Price> findAll(); // sam tworzy zapytania

    Price findByPriceId (Long id);



}
