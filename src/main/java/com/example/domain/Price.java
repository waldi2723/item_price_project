package com.example.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.Date;

/**
 * Created by wstawinski on 23.02.2017.
 */
@Entity
public class Price {


    @Id
    @GeneratedValue(generator = "PriceIdSeq")
    @SequenceGenerator(name="PriceIdSeq",sequenceName="PriceIdSeq", allocationSize=1)

    private Long priceId;


    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ItemId")
    private Item item;


    //private double value;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Timestamp dateUtc;

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Timestamp getDate() {
        return dateUtc;
    }

    public void setDate(Timestamp date) {
        this.dateUtc = date;
    }


}
