package com.example.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wstawinski on 23.02.2017.
 */

@Entity

public class Item {


    @Id
    @GeneratedValue(generator = "ItemIdSeq")
    @SequenceGenerator(name = "ItemIdSeq", sequenceName = "ItemIdSeq", allocationSize = 1)

    private Long itemId;


    @OneToMany(mappedBy = "item")
    private List<Price> prices = new ArrayList<>();

    @NotNull
    private String description;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
