package com.example.dto;

import com.example.domain.Item;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.security.Timestamp;

/**
 * Created by wstawinski on 24.02.2017.
 */
public class PriceDTO {


    private Long priceId;



    private Item item;


    private Timestamp date;

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }


}


