package com.example.dto;

import com.example.domain.Price;


import java.util.List;

/**
 * Created by wstawinski on 24.02.2017.
 */
public class ItemDTO {
    private Long itemId;

    private List<PriceDTO> pricesId;

    private String description;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public List<PriceDTO> getPricesId() {
        return pricesId;
    }

    public void setPricesId(List<PriceDTO> pricesId) {
        this.pricesId = pricesId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


